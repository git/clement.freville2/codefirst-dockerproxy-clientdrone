FROM alpine:3.19 AS builder

RUN apk add --no-cache zig --repository=https://dl-cdn.alpinelinux.org/alpine/edge/community

WORKDIR /work
COPY src src
COPY build.zig build.zig

RUN zig build -Doptimize=ReleaseSafe

FROM scratch AS runner
COPY --from=builder /work/zig-out/bin/codefirst-dockerproxy-clientdrone /
ENTRYPOINT [ "/codefirst-dockerproxy-clientdrone" ]
