const std = @import("std");
const validate = @import("validate.zig");
const allocator = std.heap.page_allocator;
var client: std.http.Client = .{ .allocator = allocator };
const Command = enum { list, logs, create, delete, start };

const ForwardedUser = "X-Forwarded-User";
const PluginError = error{ InvalidUser, InvalidCommand, NoContainerName, InvalidImage, Unprocessable };

const CodefirstAuth = struct {
    proxyScheme: []const u8,
    proxyHost: []const u8,
    user: []const u8,
};

const CodefirstContainer = struct {
    Id: []const u8,
    Image: []const u8,
    Admins: []const u8,
    Env: [][]const u8,
    Private: ?bool,
};

const jsonOptions = std.json.StringifyOptions{ .emit_null_optional_fields = false };

/// Sends a request to the CodeFirst proxy and print the response body.
fn sendRequest(method: std.http.Method, auth: CodefirstAuth, path: []const u8, body: ?[]const u8) !std.http.Status {
    var response_body = std.ArrayList(u8).init(allocator);
    const headers = [_]std.http.Header{.{ .name = ForwardedUser, .value = auth.user }};
    const res = try client.fetch(.{
        .method = method,
        .location = .{ .url = path },
        .headers = .{ .content_type = if (body != null) .{ .override = "application/json" } else .default },
        .extra_headers = &headers,
        .payload = body,
        .response_storage = .{ .dynamic = &response_body },
    });
    if (res.status.class() == .success) {
        std.log.info("{s}", .{response_body.items});
    } else {
        std.log.err("[HTTP {d}] {s}", .{ res.status, response_body.items });
    }
    return res.status;
}

/// Tests if a container with the given name exists.
fn exists(auth: CodefirstAuth, containerName: []const u8) !bool {
    const path = try std.fmt.allocPrint(allocator, "{s}://{s}/containers/{s}/json", .{ auth.proxyScheme, auth.proxyHost, containerName });
    defer allocator.free(path);
    const status = try sendRequest(.GET, auth, path, null);
    return status == .ok;
}

/// Pulls the given image.
fn createImage(auth: CodefirstAuth, imageName: []const u8) !void {
    const path = try std.fmt.allocPrint(allocator, "{s}://{s}/images/create?fromImage={s}", .{ auth.proxyScheme, auth.proxyHost, imageName });
    defer allocator.free(path);
    _ = try sendRequest(.POST, auth, path, null);
}

/// Creates a container with the given name.
fn create(auth: CodefirstAuth, container: CodefirstContainer, containerName: []const u8) !void {
    const path = try std.fmt.allocPrint(allocator, "{s}://{s}/containers/create/{s}", .{ auth.proxyScheme, auth.proxyHost, containerName });
    defer allocator.free(path);
    var json = std.ArrayList(u8).init(allocator);
    defer json.deinit();
    try std.json.stringify(container, jsonOptions, json.writer());
    _ = try sendRequest(.POST, auth, path, json.items);
}

/// Starts a container with the given options.
fn start(auth: CodefirstAuth, imageName: []const u8, containerName: []const u8, env: [][]const u8, private: bool) !void {
    const ContainerStart = struct {
        Id: []const u8,
        Image: []const u8,
        Env: [][]const u8,
        Private: ?bool,
    };
    const command = ContainerStart{
        .Id = "",
        .Image = imageName,
        .Env = env,
        // Workaround for the proxy treating 'false' as a private container.
        .Private = switch (private) {
            true => true,
            false => null,
        },
    };
    const path = try std.fmt.allocPrint(allocator, "{s}://{s}/containers/{s}/start", .{ auth.proxyScheme, auth.proxyHost, containerName });
    defer allocator.free(path);
    var json = std.ArrayList(u8).init(allocator);
    defer json.deinit();
    try std.json.stringify(command, jsonOptions, json.writer());
    _ = try sendRequest(.POST, auth, path, json.items);
}

fn delete(auth: CodefirstAuth, containerName: []const u8) !void {
    const path = try std.fmt.allocPrint(allocator, "{s}://{s}/containers/{s}", .{ auth.proxyScheme, auth.proxyHost, containerName });
    defer allocator.free(path);
    _ = try sendRequest(.DELETE, auth, path, null);
}

pub fn run() !void {
    const env_map = std.process.getEnvMap(allocator) catch unreachable;
    const command_name = env_map.get("PLUGIN_COMMAND") orelse return PluginError.InvalidCommand;
    const command = std.meta.stringToEnum(Command, command_name) orelse return PluginError.InvalidCommand;
    const auth = CodefirstAuth{
        .proxyScheme = env_map.get("PROXYSCHEME") orelse "http",
        .proxyHost = env_map.get("PROXYHOST") orelse "dockerproxy:8080",
        .user = env_map.get("DRONE_REPO_OWNER") orelse return PluginError.InvalidUser,
    };
    const containerNameEnv = env_map.get("PLUGIN_CONTAINER") orelse return PluginError.NoContainerName;
    const shortUser = try std.mem.replaceOwned(u8, allocator, auth.user, ".", "");
    defer allocator.free(shortUser);
    const containerName = try std.fmt.allocPrint(allocator, "{s}-{s}", .{ shortUser, containerNameEnv });
    defer allocator.free(containerName);

    var idx: usize = 0;
    validate.validateDockerContainerName(containerName, &idx) catch |err| {
        switch (err) {
            validate.NamingError.IllegalChar => {
                std.log.err("Invalid container name at index {}: {}", .{ idx, err });
            },
            else => {
                std.log.err("Invalid container name: {}", .{err});
                return PluginError.Unprocessable;
            },
        }
        return PluginError.Unprocessable;
    };

    var envs = std.ArrayList([]const u8).init(allocator);
    defer envs.deinit();
    for (std.os.environ) |env| {
        if (std.mem.startsWith(u8, std.mem.span(env), "CODEFIRST_CLIENTDRONE_ENV_")) {
            try envs.append(std.mem.span(env)["CODEFIRST_CLIENTDRONE_ENV_".len..]);
        }
    }

    switch (command) {
        .create => {
            const imageName = env_map.get("PLUGIN_IMAGE") orelse return PluginError.InvalidImage;
            const private = std.mem.eql(u8, env_map.get("PLUGIN_PRIVATE") orelse "false", "true");
            const overwrite = std.mem.eql(u8, env_map.get("PLUGIN_OVERWRITE") orelse "false", "true");
            const admins = env_map.get("PLUGIN_ADMINS") orelse auth.user;

            const container = CodefirstContainer{
                .Id = "",
                .Image = imageName,
                .Admins = admins,
                .Env = envs.items,
                // Workaround for the proxy treating 'false' as a private container.
                .Private = switch (private) {
                    true => true,
                    false => null,
                },
            };

            if (overwrite) {
                try delete(auth, containerName);
            }
            if (!try exists(auth, containerName)) {
                try createImage(auth, imageName);
                try create(auth, container, containerName);
                try start(auth, imageName, containerName, envs.items, private);
            }
        },
        .start => {
            const imageName = env_map.get("PLUGIN_IMAGE") orelse return PluginError.InvalidImage;
            const private = std.mem.eql(u8, env_map.get("PLUGIN_PRIVATE") orelse "false", "true");
            try start(auth, imageName, containerName, envs.items, private);
        },
        .delete => {
            try delete(auth, containerName);
        },
        else => unreachable,
    }
}

pub fn main() !void {
    run() catch |err| {
        switch (err) {
            PluginError.InvalidCommand => std.log.err("Invalid command (possible values: list|logs|create|delete|start)", .{}),
            PluginError.InvalidUser => std.log.err("Invalid user", .{}),
            PluginError.NoContainerName => std.log.err("No container name", .{}),
            PluginError.InvalidImage => std.log.err("Invalid image", .{}),
            else => std.log.err("{}", .{err}),
        }
        std.process.exit(1);
    };
}
