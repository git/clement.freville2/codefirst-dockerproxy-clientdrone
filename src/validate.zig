const std = @import("std");
const ascii = std.ascii;

pub const NamingError = error{ Empty, IllegalChar };

pub fn validateDockerContainerName(name: []const u8, error_index: *usize) !void {
    // [a-zA-Z0-9][a-zA-Z0-9_.-]
    if (name.len == 0) {
        return NamingError.Empty;
    }

    if (!ascii.isAlphanumeric(name[0])) {
        error_index.* = 0;
        return NamingError.IllegalChar;
    }
    for (name, 0..) |c, idx| {
        if (!ascii.isAlphanumeric(c) and c != '.' and c != '_' and c != '-') {
            error_index.* = idx;
            return NamingError.IllegalChar;
        }
    }
}

test "valid name" {
    var error_index: usize = 0;
    try validateDockerContainerName("hello", &error_index);
    try std.testing.expect(error_index == 0);
}

test "indicate non ascii letter" {
    var error_index: usize = 0;
    try std.testing.expectError(NamingError.IllegalChar, validateDockerContainerName("hello😀", &error_index));
    try std.testing.expect(error_index == 5);
}
