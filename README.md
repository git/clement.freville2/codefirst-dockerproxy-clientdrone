# codefirst-dockerproxy-clientdrone

Usage:

```yaml
  - name: deploy-container
    image: hub.codefirst.iut.uca.fr/clement.freville2/codefirst-dockerproxy-clientdrone:latest
    settings:
      image: hub.codefirst.iut.uca.fr/my.login/my_repository:latest
      container: my_container_name
      command: create
      overwrite: true
```

## Changes from upstream

- Removed the shell script to handle spaces in arguments.
- Used Drone settings instead of environment variables.
